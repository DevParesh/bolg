<li class="nav-item ">
    <a href="{{route('blogs.index')}}" class="nav-link {{ Request::is('blogs*') ? 'active' : '' }}">
        <i class="nav-icon fas fa-tachometer-alt"></i>
        <p>Blog</p>
    </a>
</li>
<li class="nav-item">
        <a href="{{route('category.index')}}" class="nav-link {{ Request::is('category*') ? 'active' : '' }}">
        <i class="nav-icon fas fa-tachometer-alt"></i>
        <p>Category</p>
    </a>
</li>
