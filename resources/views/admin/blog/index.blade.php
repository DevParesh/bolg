@extends('admin.layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Blogs</h1>
                </div>
                <div class="col-sm-6">
                        <a class="btn btn-primary float-right"
                           href="{{ route('blogs.create') }}">
                            Add New
                        </a>
            </div>
            </div>

        </div>
    </section>

    <div class="content px-3">
        <form method="GET">
        <div class="row">
            <div class="col-sm-3">
             <input data-date-format="dd/mm/yyyy" class="form-control" name="date" id="datepicker">
             </div>
            <div class="col-sm-2">
                <button type="submit" class="btn btn-primary">Filter</button>
            </div>
        </div>
        </form>

        @include('flash-message')

        <div class="clearfix"></div>

        <div class="card">
            <div class="card-body p-0">
                @include('admin.blog.table')

                <div class="card-footer clearfix">
                    <div class="float-right">
                        {!! $blogs->withQueryString()->links('pagination::bootstrap-5') !!}
                    </div>
                </div>
            </div>

        </div>
    </div>

@endsection

