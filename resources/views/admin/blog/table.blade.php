<div class="table-responsive">
    <table class="table" id="batchTypes-table">
        <thead>
        <tr>
            <th>ID</th>
            <th>Title</th>
            <th>Image</th>
            <th>Status</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>

        @if(count($blogs)>0)
        @foreach($blogs as $blog)
            <tr>
                <td>{{ $blog->id }}</td>
                <td>{{ $blog->title }}</td>
                <td><img alt="image" src="{{asset('storage/image/'.$blog->image)}}" style="width: 106px;height: 80px;"></td>
                <td><span class='badge @if($blog->status == 1)badge-success @else badge-danger @endif'>{{ $blog->status == 1 ? "Active" : "Block" }}</span></td>
                <td width="120">
                    {!! Form::open(['route' => ['blogs.destroy', $blog->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger action-btn btn-sm', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
            @else

            <tr>
                <td colspan="3">There are no blogs.</td>
            </tr>

            @endif
        </tbody>
    </table>
</div>
