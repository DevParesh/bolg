<!-- Title Field -->
<div class="form-group col-sm-6">
    {!! Form::label('title', 'Title:') !!}
    {!! Form::text('title', null, ['class' => 'form-control']) !!}
    <span class="error text-danger">{{ $errors->first('title') }}</span>
</div>
<div class="form-group col-sm-6">
    {!! Form::label('image', 'Image:') !!}<br>
    {!! Form::file('image', null, ['class' => 'form-control']) !!}
    <span class="error text-danger">{{ $errors->first('image') }}</span>
</div>
<div class="form-group col-sm-6">
    {!! Form::label('description', 'Description:') !!}
    {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-12">
    {!! Form::label('status', 'Status:') !!}
    <input type="checkbox" id="status" name="status" value="1">
</div>
