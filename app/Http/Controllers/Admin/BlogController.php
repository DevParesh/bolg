<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\CreateBlogRequest;
use App\Models\Blog;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;



class BlogController extends AppBaseController
{

    public function index(Request $request)
    {
        $blogs= Blog::paginate(2);
       if (\request('date')){
           $date = \Illuminate\Support\Facades\Date::make(\request('date'))->format('Y-d-m');
           $blogs= Blog::whereDate('created_at', '=',$date)->get();
       }
        return view('admin.blog.index',compact('blogs'));
    }

    public function create()
    {
        return view('admin.blog.create');
    }

    public function store(CreateBlogRequest $request)
    {
        $input = $request->all();
        if ($request->hasFile("image")) {
            $img = $request->file("image");
            $img->store('public/image');
            $input['image'] = $img->hashName();
        }
        $blog = Blog::create($input);
        return redirect(route('blogs.index'))->with('success','Blog saved successfully');
    }
    public function show($id)
    {

    }

    public function edit($id)
    {
    }
    public function update()
    {

    }


    public function destroy($id)
    {
        $blog = Blog::find($id);
        if (empty($blog)) {
            return redirect(route('blogs.index'))->with('danger','Blog not found');
        }
        $blog->delete();
        return redirect(route('blogs.index'))->with('success','Blog deleted successfully');
    }
}
