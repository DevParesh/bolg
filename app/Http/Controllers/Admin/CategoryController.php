<?php

namespace App\Http\Controllers\Admin;




use App\DataTables\Admin\CategoryDataTable;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;

class CategoryController extends AppBaseController
{
    public function index(CategoryDataTable $categoryDataTable)
    {

        return $categoryDataTable->render('admin.category.index');
    }

    public function show($id) {

    }

    public function store() {

    }
}
