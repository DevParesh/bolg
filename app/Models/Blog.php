<?php

namespace App\Models;

use Eloquent as Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Blog extends Model
{
    public $table = 'blogs';




    public $fillable = [
        'title',
        'image',
        'status',
        'description',
    ];

    protected $casts = [
        'id' => 'integer',
        'title' => 'string',
        'image' => 'string',
        'status' => 'integer',
        'description' => 'string',
    ];

    public static $rules = [
        'title' => 'required|unique:blogs,title',
        'image' => 'required|image|mimes:jpg,png,jpeg',
    ];


}
