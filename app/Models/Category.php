<?php

namespace App\Models;

use Eloquent as Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Category extends Model
{
    public $table = 'categories';




    public $fillable = [
        'category_name',
        'blog_id',
        'status',
        'description',
    ];

    protected $casts = [
        'id' => 'integer',
        'category_name' => 'string',
        'blog_id' => 'integer',
        'status' => 'integer',
        'description' => 'string',
    ];

    public static $rules = [
        'category_name' => 'required',
        'blog_id' => 'required',
    ];

    public function blog()
    {
        return $this->belongsTo(Blog::class, 'blog_id');
    }
}
