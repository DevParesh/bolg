<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\BlogController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return \route('blogs.index');
//});
Route::get('/',[App\Http\Controllers\Admin\BlogController::class,'index']);
Route::resource('blogs', App\Http\Controllers\Admin\BlogController::class);
Route::resource('category',App\Http\Controllers\Admin\CategoryController::class);

