<?php

namespace Database\Seeders;

use App\Models\Blog;
use App\Models\Category;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::create([
            'category_name' => 'Cricket',
            'blog_id' => '5',
            'status' => '1',
            'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. ',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        Category::create([
            'category_name' => 'Football',
            'blog_id' => '5',
            'status' => '1',
            'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. ',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        Category::create([
            'category_name' => 'Alto',
            'blog_id' => '6',
            'status' => '1',
            'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. ',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        Category::create([
            'category_name' => 'Alto 800',
            'blog_id' => '6',
            'status' => '1',
            'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. ',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);


    }
}
